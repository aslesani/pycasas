import os
import sys
import copy
import json
import logging
import humanize
import xlsxwriter
import progressbar
import numpy as np
import dateutil.parser
from datetime import datetime
from datetime import time
from collections import OrderedDict
from ._site import CASASSite
from ._sensor import CASASSensorType


logger = logging.getLogger(__name__)


class CASASDataset:
    r"""CASAS Dataset Class

    CASAS Dataset Class stores and processes all information regarding a
    recorded smart home dataset.

    A dataset is usually composed of a meta-data json file and a list of .csv
    file containing recorded sensor events.
    The meta-data about the dataset (dataset.json) is parsed and loaded into
    the ``data_dict`` attribute.
    Usually, it is a dictionary consisted of the following keys:

    * ``name``: name of the dataset.
    * ``activities``: list of activities (dictionary structure) tagged in the
      dataset.
    * ``residents``: list of residents (dictionary structure) tagged in the
      dataset.
    * ``site``: name of the site where this dataset is recorded.

    ``events.csv`` file within the dataset directory contains binary sensor
    events triggered by motion sensors, area motion sensors, item sensors,
    and door sensors. Actually, any sensor that reports "ON", "OFF", "OPEN",
    "CLOSE", "ABSENT" or "PRESENT" as their message are considered binary event
    sensors and are recorded sequentially within the file.
    Most of the algorithm and methods implemented in this library consume these
    binary sensor events to infer the possible trace of each resident and infer
    the activity they are performing at a given time slot.

    The ``event.csv`` file is composed of 6 columns separated by comma.
    Here are those columns:

    * **time tag**: MM/DD/YYYY HH:mm:ss with time zone information represents
      unambiguously a specific time spot where the sensor event
      occurs, e.g. 12/01/2016 00:01:41 -08:00
    * **sensor name**: the target name of the sensor that triggers the event.
    * **sensor message**: the message that the sensor reports. For motion
      sensor, it is either "ON" or "OFF". Door sensor sends "OPEN" or
      "CLOSE", while item sensor sends "PRESENT" or "ABSENT".
    * **resident names**: The name/alias of the resident that triggers the
      sensor event and message. If multiple residents are the cause of the
      sensor event, they are separated by ``;``.
    * **activity name**: The name/label of the activity that the resident who
      triggers the sensor event is performing. If the sensor is triggered by
      multiple residents and they are performing different activities,
      the activities labels are separated by ``;``.
    * **comments**: Extra comments about this sensor event. Usually it contains
      the sensor type there.

    The ``event.csv`` file is loaded with :meth:`load_event` function. All the
    loaded events are stored in ``event_list`` attributes. Each entry in the
    list is a dictionary composed of the following key-value pairs:

    * ``datetime``: a datetime structure representing a specific time spot.
    * ``sensor``: the target name of the sensor.
    * ``message``: the message that the sensor sends.
    * ``resident``: the name/alias of the resident if tagged.
    * ``activity``: the name/label of the acitivity if tagged.

    Attributes:
        data_dict (:obj:`dict`):
            A dictionary contains information about smart home.
        directory (:obj:`str`): Path to the directory that stores CASAS smart
            home data.
        site (:obj:`pymrt.casas.site.CASASSite`):
            The :obj:`CASASSite` class representing the smart home site
            information.
        activity_dict (:obj:`dict`): A dictionary indexed by the name of
            activity for faster activity lookup. Each activity is a
            :obj:`dict` structure composed of key ``name`` and ``color``.
        resident_dict (:obj:`dict`): A dictionary indexed by the name of
            resident living in the smart home for faster resident lookup.
            Each resident is a :obj:`dict` structure composed of key ``name``
            and ``color``.
        activity_indices_dict (:obj:`dict`): A dictionary indexed by the
            activity name where the activity is assigned an index value to be
            used in activity recognition algorithm.
        enabled_sensor_types (:obj:`dict`): A dictionary indexed by enabled
            sensor types. The corresponding value is a :obj:`list` of sensor
            names that belongs to the sensor type index. It is used in
            load_event function to provide the functionality of enable or
            disable specific sensors.
        sensor_list (:obj:`list`): List of sensors enabled. It will be
            populated after the dataset is loaded.
        sensor_indices_dict (:obj:`dict`): A dictionary indexed by sensor name
            with the index of the corresponding sensor as its value. It is
            used for fast look-up of sensor index during data pre-processing and
            post-processing for activity recognition and multi-resident
            tracking.
        event_list (:obj:`list`): List of loaded events.
        stats (:obj:`dict`): Dictionary containing simple statistics about
            the dataset.

    Parameters:
        directory (:obj:`str`): Directory that stores CASAS smart home data
    """
    def __init__(self, directory):
        # Sanity checks
        if not os.path.isdir(directory):
            raise NotADirectoryError('%s is not a directory.' % directory)
        dataset_json_fname = os.path.join(directory, 'data', 'dataset.json')
        if not os.path.exists(dataset_json_fname):
            raise FileNotFoundError('CASAS dataset meta-data file '
                                    '\'data.json\' is not found under '
                                    'directory %s. Please check if the '
                                    'directory provided is correct.' %
                                    directory)
        # Finished check - Start loading data from CASAS dataset
        self.directory = directory
        f = open(dataset_json_fname, 'r')
        self.data_dict = json.load(f)
        self.site = self._get_site(site_dir=os.path.join(directory, 'site'))
        self.activity_dict = OrderedDict([(activity['name'], activity)
                                          for activity in self.data_dict['activities']])
        self.resident_dict = self._get_residents()
        # Generate indices for each activity
        self.activity_indices_dict = self._get_activity_indices()
        self.enabled_sensor_types = self.site.get_all_sensor_types()
        # Sensor list and indices dict populated after dataset is loaded
        self.sensor_list = []
        self.sensor_indices_dict = {}
        self.event_list = []
        # Simple statistics
        self.stats = {}
        # Additional information about the dataset
        self.description = self.data_dict['description'] if 'description' in self.data_dict else ""

    def enable_sensor_with_sensor_categories(self, categories):
        """Load only certain type of sensors in the dataset if its sensor
        category is specified.
        """
        all_types = self.site.get_all_sensor_types()
        enabled_sensor_types = {}
        for sensor_type in all_types:
            sensor_category = CASASSensorType.get_best_category_for_sensor(
                [sensor_type]
            )
            if sensor_category in categories:
                enabled_sensor_types[sensor_type] = all_types[sensor_type]
        if len(enabled_sensor_types) == 0:
            logger.error('No sensors found that fits the categories provided.')
            return
        self.enabled_sensor_types = enabled_sensor_types

    def enable_sensor_with_types(self, type_array):
        """Load only certain type of sensors in the dataset
        """
        all_types = self.site.get_all_sensor_types()
        enabled_sensor_types = {}
        num_errors = 0
        for sensor_type in type_array:
            if sensor_type in all_types:
                enabled_sensor_types[sensor_type] = all_types[sensor_type]
            else:
                logger.error('Sensor type %s not found in site %s' % (
                sensor_type, self.site.get_name()))
                num_errors += 1
        if num_errors == 0:
            self.enabled_sensor_types = enabled_sensor_types

    def _get_residents(self):
        """Analyze residents information from loaded meta-data.

        Note that self.data_dict needs to be populated from the json file first
        before this method is called.
        """
        resident_dict = {}
        multi_resident_names = []
        for resident in self.data_dict['residents']:
            if ';' in resident['name']:
                multi_resident_names.append(resident['name'])
            else:
                resident_dict[resident['name']] = resident
        # Check and see if multi-resident names are composed of residents listed
        # in the meta-data
        for multi_resident_name in multi_resident_names:
            name_list = multi_resident_name.split(';')
            for name in name_list:
                if name not in resident_dict:
                    logger.warning('Resident %s in name %s does not exist in '
                                   'the meta-data. Please check the data '
                                   'integrity of the annotated dataset.' %
                                   (name, multi_resident_names))
        # Return the dictionary
        return resident_dict

    def _get_site(self, site_dir=None, site=None):
        """Returns the smart home site

        Args:
            site_dir (:obj:`str`): Path to the CASAS site directory where this
                dataset is recorded.
            site (:obj:`CASASSite`): The CASASSite structure containing the
                information about this dataset.
        """
        if type(site) is CASASSite:
            if self.data_dict['site'] == site.get_name():
                return site
        if site_dir is not None and os.path.isdir(site_dir):
            possible_site_path = site_dir
        else:
            from . import casas_sites_dir
            if casas_sites_dir is not None:
                possible_site_path = os.path.join(casas_sites_dir,
                                                  self.data_dict['site'])
            else:
                raise RuntimeError("Cannot load site specified in the dataset.")
        return CASASSite(directory=possible_site_path)

    def get_name(self):
        """Get the smart home name

        Returns:
            :obj:`str`: smart home name
        """
        return self.data_dict['name']

    def get_all_activity_names(self):
        """Get All Activities

        Returns:
            :obj:`list` of :obj:`str`: list of activity names
        """
        names = [activity['name'] for activity in self.data_dict['activities']]
        return names

    def get_activity(self, label):
        """Find the information about the activity

        Parameters:
            label (:obj:`str`): activity label

        Returns:
            :obj:`dict`: A dictionary containing activity information
        """
        return self.activity_dict.get(label, None)

    def get_activity_color(self, label):
        """Find the color string of the activity

        Parameters:
            label (:obj:`str`): activity label

        Returns:
            :obj:`str`: RGB color string
        """
        activity = self.get_activity(label)
        if activity is not None:
            return "#" + activity['color'][3:9]
        else:
            raise ValueError('Activity %s Not Found' % label)

    def get_resident(self, name):
        """Get Information about the resident

        Parameters:
            name (:obj:`str`): name of the resident

        Returns:
            :obj:`dict`: A Dictionary that stores resident information
        """
        return self.resident_dict.get(name, None)

    def get_resident_color(self, name):
        """Get the color string for the resident

        Parameters:
            name (:obj:`str`): name of the resident

        Returns:
            :obj:`str`: RGB color string representing the resident
        """
        resident = self.get_resident(name)
        if resident is not None:
            return "#" + resident['color'][3:9]
        else:
            raise ValueError('Resident %s Not Found' % name)

    def get_all_resident_names(self):
        """Get All Resident Names

        Returns:
            :obj:`list` of :obj:`str`: A list of resident names
        """
        names = [resident['name'] for resident in self.data_dict['residents']]
        return names

    def _get_sensor_indices(self):
        """Get a dictionary for sensor id look-up.
        """
        self.sensor_indices_dict = {}
        for key, sensor in enumerate(self.sensor_list):
            self.sensor_indices_dict[sensor['name']] = key

    def _get_activity_indices(self):
        """Get a dictionary for activity id look-up
        """
        activity_indices_dict = {}
        for key, activity in enumerate(self.data_dict['activities']):
            activity_indices_dict[activity['name']] = key
        return activity_indices_dict

    def _get_resident_indices(self):
        """Get a dictionary for residents id look-up
        """
        resident_indices_dict = {}
        index = 0
        for resident_name in self.resident_dict:
            resident_indices_dict[resident_name] = index
            index += 1
        return resident_indices_dict

    def load_events(self, show_progress=True):
        """Load events from CASAS event.csv file

        Args:
            show_progress (:obj:`bool`): Show progress of event loading
        """
        # Sanity check
        events_fname = os.path.join(self.directory, 'data', 'events.csv')
        if not os.path.isfile(events_fname):
            raise FileNotFoundError('Sensor event records not found in CASAS '
                                    'dataset %s. Check if event.csv exists '
                                    'under directory %s.' % (self.get_name(),
                                                             self.directory))
        if show_progress:
            file_size = os.path.getsize(events_fname)
            sys.stdout.write('Loading events from events.csv. Total size: ' +
                             humanize.naturalsize(file_size, True) + '\n')
            chunk_size = file_size / 100
            size_loaded = 0
            loaded_percentage = 0
        # Clear event_list
        self.event_list = []
        # Initialize supporting data structure
        # Record all sensor names that is valid and presented in the event file
        #  loaded. Contain all valid sensor names from the site, use dict for
        #  faster search.
        valid_sensor_names = {key: None for key in
                              self.site.get_all_sensor_names()}
        # List of sensors that appeared in sensor events but are not found in
        #  valid sensor names dictionary
        sensors_notfound_list = {}
        # Records all the enabled valid sensors that has one or more than one
        #  record in the sensor event file.
        logged_sensor_names = {}
        # A set contains enabled sensor names.
        enabled_sensor_names = set()
        for sensor_type, sensor_names in self.enabled_sensor_types.items():
            enabled_sensor_names = enabled_sensor_names.union(sensor_names)
        # Residents that are tagged in the dataset
        logged_residents = {}
        residents_notfound_list = {}
        # Activities that are tagged in the dataset
        logged_activities = {}
        activities_notfound_list = {}

        # Start reading event file. The process loads all sensor events into
        #  self.event_list. If the sensor reporting the event is enabled,
        #  the event is appended to the list. If it is disabled, the event is
        #  skipped. If the sensor cannot be found in the meta-data of smart
        #  home site, an error is reported and the event is skipped.
        f = open(events_fname, 'r')
        line_number = 0
        for line in f:
            line_number += 1
            word_list = str(str(line).strip()).split(',')
            if len(word_list) < 3:
                # If not enough item (at least 3) in the entry for a sensor
                #  event, report error and continue.
                logger.error(
                    'Error parsing %s:%d' % (events_fname, line_number))
                logger.error('  %s' % line)
                continue
            # Parse datetime
            event_time = dateutil.parser.parse(word_list[0])
            # Check sensor name
            event_sensor = word_list[1]
            if event_sensor not in valid_sensor_names:
                # If event sensor is not found in the site information,
                # record the issue and continue.
                if event_sensor not in sensors_notfound_list:
                    sensors_notfound_list[event_sensor] = 1
                    logger.warning(
                        'Sensor name %s not found in home metadata' %
                        event_sensor)
                sensors_notfound_list[event_sensor] += 1
                continue
            # If sensor name is found, and its type is enabled, log its name.
            if event_sensor in enabled_sensor_names:
                if event_sensor not in logged_sensor_names:
                    logged_sensor_names[event_sensor] = 0
                else:
                    logged_sensor_names[event_sensor] += 1
            else:
                # The sensor is disabled, skip to next event.
                continue
            event_message = word_list[2]
            # Parse resident name and activity label
            # In order to accommodate multi-resident scenario, resident and
            # activities can both be an array.
            if len(word_list) > 3:
                # Parse residents
                residents = word_list[3] if word_list[3] != "" else None
                # Check if the resident name is legit
                if residents is not None:
                    residents = residents.split(';')
                    # Check if all residents are valid
                    for i in range(len(residents) - 1, -1, -1):
                        if residents[i] not in self.resident_dict:
                            resident = residents.pop(i)
                            if resident not in residents_notfound_list:
                                residents_notfound_list[resident] = 0
                                logger.warning(
                                    "Resident %s is not found in resident list."
                                    % resident)
                            else:
                                residents_notfound_list[resident] += 1
                        else:
                            resident = residents[i]
                            if resident in logged_residents:
                                logged_residents[resident] += 1
                            else:
                                logged_residents[resident] = 0
                else:
                    residents = []
            else:
                residents = []

            if len(word_list) > 4:
                # Activities list
                activities = word_list[4] if word_list[4] != "" else None
                # Check if the resident name is legit
                if activities is not None:
                    activities = activities.split(';')
                    # Check if all residents are valid
                    for i in range(len(activities) - 1, -1, -1):
                        if activities[i] not in self.activity_dict:
                            activity = activities.pop(i)
                            if activity not in activities_notfound_list:
                                activities_notfound_list[activity] = 0
                                logger.warning(
                                    "Activity %s is not found in activity list."
                                    % activity)
                            else:
                                activities_notfound_list[activity] += 1
                        else:
                            if activities[i] in logged_activities:
                                logged_activities[activities[i]] += 1
                            else:
                                logged_activities[activities[i]] = 0
                else:
                    activities = []
            else:
                activities = []

            cur_data_dict = {
                'datetime': event_time,
                'sensor': event_sensor,
                'message': event_message,
                'resident': residents,
                'activity': activities
            }

            # Add Corresponding Labels
            self.event_list.append(cur_data_dict)
            if show_progress:
                # Figure out a way of showing progress
                size_loaded += len(line)
                if size_loaded > chunk_size:
                    loaded_percentage += int(size_loaded / chunk_size)
                    size_loaded = size_loaded % chunk_size
                    sys.stdout.write("\rProgress: %d%%" %
                                     int(loaded_percentage))
        if show_progress:
            sys.stdout.write("\rProgress: 100%%\n")
        # Finished reading the whole file, create sensor list
        for sensor in self.site.sensor_list:
            if sensor['name'] in logged_sensor_names:
                self.sensor_list.append(sensor)
        self._get_sensor_indices()

    def to_sensor_sequence(self, ignore_off=True):
        """Return a sensor sequence based on event list.

        The function ignores the `OFF`, `ABSENT` or `CLOSE` sensor event and
        returns a `list` of sensor IDs and time tag associated with each event.

        Args:
            ignore_off (:obj:`bool`): Ignore `OFF`, `ABSENT` or `CLOSE` event.

        Returns:
            sensor_seq, time_seq: `list` objects, where the `sensor_seq` is a
                `list` of sensor sequence, and `time_seq` is a list of
                `datetime` object representing the time tag of corresponding
                sensor events.
        """
        sensor_seq, activity_seq, time_seq = self._generate_sensor_sequence(
            ignore_off=ignore_off
        )
        return sensor_seq, time_seq

    def to_observation_track(self, show_progress=True):
        """Generate observation list based on the sensor events loaded.

        Turn a list of sensor events into a list of observation every time a
        sensor is triggered or in active state. Each observation is represented
        as a `dict`, with each measurement (in this case, sensor ID) as its
        `key` and its value is the information of the measurement.

        The information of each measurement is represented as a `dict` as well,
        with the following key-value pairs:
        - `start`: Start `datetime`
        - `stop`: Stop `datetime`
        - `residents`: List of residents associated with the sensor events.

        Args:
            show_progress (:obj:`bool`): Show observation track generation
                progress.

        Returns:
            obs_seq, time_seq: A two-`tuple` with a `list` of observation and
                a `list` of `datetime` time tags associated with each
                observation.
        """
        obs_seq, activity_seq, time_seq = self._generate_sensor_observations()
        return obs_seq, time_seq

    def get_observation_resident_association(self):
        """Get observation and resident association based on sensor events
        """
        sensor_status = {self.sensor_indices_dict[sensor['name']]: 0
                         for sensor in self.sensor_list}
        sensor_occupancy = {self.sensor_indices_dict[sensor['name']]: []
                            for sensor in self.sensor_list}
        observation_resident_association = []
        time_seq = []
        for i, event in enumerate(self.event_list):
            sensor = self.sensor_indices_dict[event['sensor']]
            if event['message'] == "OFF" or event['message'] == 'ABSENT' or\
                    event['message'] == 'CLOSE':
                sensor_status[sensor] = 0
            else:
                sensor_occupancy[sensor] = event['resident']
                sensor_status[sensor] = 1
                current_observation = {}
                for sensor in sensor_status:
                    if sensor_status[sensor] == 1:
                        current_observation[sensor] = \
                            sensor_occupancy[sensor]
                observation_resident_association.append(current_observation)
                time_seq.append(event['datetime'])
        return observation_resident_association, time_seq

    def get_resident_to_measurement_association(self):
        """Get each resident to measurement association
        """
        sensor_status = {self.sensor_indices_dict[sensor['name']]: 0
                         for sensor in self.sensor_list}
        sensor_occupancy = {self.sensor_indices_dict[sensor['name']]: []
                            for sensor in self.sensor_list}
        resident_measurement_association = {
            resident: [] for resident in self.resident_dict
        }
        time_seq = []
        for i, event in enumerate(self.event_list):
            sensor = self.sensor_indices_dict[event['sensor']]
            if event['message'] == "OFF" or event['message'] == 'ABSENT' or\
                    event['message'] == 'CLOSE':
                sensor_status[sensor] = 0
            else:
                sensor_occupancy[sensor] = event['resident']
                sensor_status[sensor] = 1
                for resident in resident_measurement_association:
                    measurements = []
                    for sensor in sensor_status:
                        if sensor_status[sensor] == 1 and resident in \
                                sensor_occupancy[sensor]:
                            measurements.append(sensor)
                    resident_measurement_association[resident].append(
                        measurements
                    )
                time_seq.append(event['datetime'])
        return resident_measurement_association, time_seq

    def get_sensor_sequence_by_resident(self, ignore_off=True):
        sensor_sequence_by_resident = {
            resident: [] for resident in self.resident_dict
        }
        time_seq_by_resident = {
            resident: [] for resident in self.resident_dict
        }
        for event in self.event_list:
            sensor = self.sensor_indices_dict[event['sensor']]
            if ignore_off and \
                    event['message'] == "OFF" or \
                    event['message'] == 'ABSENT' or \
                    event['message'] == 'CLOSE':
                continue
            else:
                for resident in event['resident']:
                    sensor_sequence_by_resident[resident].append(sensor)
                    time_seq_by_resident[resident].append(event['datetime'])
        return sensor_sequence_by_resident, time_seq_by_resident

    def summary(self):
        """Print Summary of the dataset class
        """
        print('==============================')
        print('Dataset: %s' % self.get_name())
        print('Location: %s' % self.directory)
        print('==============================')
        print('\t Sensor Types enabled: %s' % str(
            list(self.enabled_sensor_types.keys())))
        if len(self.event_list) == 0:
            print('\t Events not loaded')
        else:
            print('\t %d events loaded.' % len(self.event_list))
            print('\t %d sensors presented in the dataset.' % len(
                self.sensor_list))

    def info(self):
        """Print brief information about the dataset.
        """
        print('Dataset: `%s`' % self.get_name())
        print('\tpath: `%s`' % self.directory)
        print('Site: %s' % self.site.get_name())
        print('\tpath: %s' % self.site.directory)
        if len(self.event_list) == 0:
            print('Number of sensors: %s' % len(self.site.sensor_list))
            print('Sensors deployed in smart home site:')
            for s in self.site.sensor_list:
                print('\t[%s]' % s['name'])
        else:
            print('Number of enabled sensors: %d' % len(self.sensor_list))
            print('Number of sensor events: %d' % len(self.event_list))
            print('Enabled sensor types:')
            for sensor_type in self.enabled_sensor_types.keys():
                print('\t%s' % sensor_type)

    def check_event_list(self, xlsx_fname=None):
        """Check event list for issues

        Common issues are:

        1. Motion sensor, Missing ON event (two consecutive OFF)
        2. Motion sensor, Missing OFF event (two consecutive ON)
        3. Item sensor, Missing ABSENT (two consecutive PRESENT)
        4. Item sensor, Missing PRESENT (two consecutive ABSENT)
        5. Door sensor, Missing OPEN (two consecutive CLOSE)
        6. Door sensor, Missing CLOSE (two consecutive OPEN)

        Args:
            xlsx_fname (:obj:`str`): XLSX file path to store the event checking
                result. Print the result in console if it is set to None.
        """
        # dict[sensor, state] that stores last state of a sensor
        prev_state = {sensor['name']: '' for sensor in self.sensor_list}
        # dict[sensor, int] that stores the occurrence of each sensor
        total_events = {sensor['name']: 0 for sensor in self.sensor_list}
        # dict[sensor, dict[state, int]] that stores the occurrence of each
        # faults
        issues_summary = {sensor['name']: {} for sensor in self.sensor_list}
        for event in self.event_list:
            if event['message'] not in issues_summary[event['sensor']]:
                issues_summary[event['sensor']][event['message']] = 0.
            if prev_state[event['sensor']] == event['message']:
                issues_summary[event['sensor']][event['message']] += 1
                total_events[event['sensor']] += 1
            else:
                total_events[event['sensor']] += 0.5
            prev_state[event['sensor']] = event['message']
        # radio errors
        sensor_radio_errors = {}
        fp = open(os.path.join(self.directory, 'data', 'error.csv'), 'r')
        for line in fp.readlines():
            tokens = line.strip().split(',')
            sensor_name = tokens[1]
            if sensor_name in sensor_radio_errors:
                sensor_radio_errors[sensor_name] += 1
            else:
                sensor_radio_errors[sensor_name] = 1
        fp.close()
        # Report the result
        if xlsx_fname is None:
            # print on screen
            print('-------------')
            print('Issues Report')
            print('-------------')
            for sensor_name, issue_dict in issues_summary.items():
                issues = list(issue_dict.items())
                if total_events[sensor_name] < 2:
                    # Only occurred once in the dataset
                    logger.warning(
                        'Sensor %s only occurred once/twice in the whole '
                        'dataset.' % sensor_name)
                    continue
                if len(issues) < 2:
                    logger.warning(
                        'Sensor %s does not have two states in the file.' %
                        sensor_name)
                    logger.warning('Here is the issue dict:')
                    logger.warning(str(issues))
                if sensor_name in sensor_radio_errors:
                    radio_errors = sensor_radio_errors[sensor_name]
                else:
                    radio_errors = 0
                print(
                    '[%s]: total %d, '
                    '%s error %d (%.3f%%), '
                    '%s error %d (%.3f%%), '
                    'radio_error %d (%.3f%%)'
                    % (sensor_name, total_events[sensor_name],
                       issues[0][0], issues[0][1],
                       issues[0][1] * 100. / float(total_events[sensor_name]),
                       issues[1][0], issues[1][1],
                       issues[1][1] * 100. / float(total_events[sensor_name]),
                       radio_errors,
                       radio_errors * 100. / float(total_events[sensor_name])
                    ))
        else:
            import xlsxwriter
            workbook = xlsxwriter.Workbook(xlsx_fname)
            summary_sheet = workbook.add_worksheet('Summary')
            for i, text in enumerate(
                    ['Sensors', 'Total Events', 'Issues', 'Events',
                     'Percentage', 'Radio Errors', 'Radio Err Percentage']):
                summary_sheet.write(0, i, text)
            for i, name in enumerate(list(issues_summary.keys())):
                summary_sheet.merge_range(1 + 2 * i, 0, 2 + 2 * i, 0, name)
                summary_sheet.merge_range(1 + 2 * i, 1, 2 + 2 * i, 1,
                                          total_events[name])
                if total_events[name] < 2:
                    # Only occurred once in the dataset
                    logger.warning(
                        'Sensor %s only occurred once/twice in the whole '
                        'dataset.' % name)
                    # continue
                for j, issue_label in enumerate(
                        list(issues_summary[name].keys())):
                    summary_sheet.write(1 + j + 2 * i, 2, issue_label)
                    summary_sheet.write(1 + j + 2 * i, 3,
                                        issues_summary[name][issue_label])
                    summary_sheet.write(1 + j + 2 * i, 4, issues_summary[name][
                        issue_label] / float(total_events[name]))
                if name in sensor_radio_errors:
                    radio_errors = sensor_radio_errors[name]
                else:
                    radio_errors = 0
                summary_sheet.merge_range(1 + 2 * i, 5, 2 + 2 * i, 5,
                                          radio_errors)
                summary_sheet.merge_range(
                    1 + 2 * i, 6, 2 + 2 * i, 6,
                    radio_errors / float(total_events[name])
                )
            workbook.close()
            lines = []
            for sensor_name, issue_dict in issues_summary.items():
                issues = list(issue_dict.items())
                if total_events[sensor_name] < 2:
                    # Only occurred once in the dataset
                    logger.warning(
                        'Sensor %s only occurred once/twice in the whole '
                        'dataset.' % sensor_name)
                    continue
                if len(issues) < 2:
                    logger.warning(
                        'Sensor %s does not have two states in the file.' %
                        sensor_name)
                    logger.warning('Here is the issue dict:')
                    logger.warning(str(issues))
                if sensor_name in sensor_radio_errors:
                    radio_errors = sensor_radio_errors[sensor_name]
                else:
                    radio_errors = 0
                lines.append(
                    '[%s]: total %d, '
                    '%s error %d (%.3f%%), '
                    '%s error %d (%.3f%%), '
                    'radio_error %d (%.3f%%)'
                    % (sensor_name, total_events[sensor_name],
                       issues[0][0], issues[0][1],
                       issues[0][1] * 100. / float(total_events[sensor_name]),
                       issues[1][0], issues[1][1],
                       issues[1][1] * 100. / float(total_events[sensor_name]),
                       radio_errors,
                       radio_errors * 100. / float(total_events[sensor_name])
                    ))
            return '\n'.join(lines)

    def check_sensor_activation_duration(self):
        """Summary of the duration of sensor activation
        """
        start_status = ['ON', 'PRESENT', 'OPEN']
        stop_status = ['OFF', 'ABSENT', 'CLOSE']
        # dict[sensor, state] that stores last state of a sensor
        prev_state = {sensor['name']: '' for sensor in self.sensor_list}
        # dict[sensor, datetime] that stores the time of last sensor state
        # report
        prev_time = {sensor['name']: None for sensor in self.sensor_list}
        # dict[sensor, list of int] that stores the duration summary for each
        # sensor firing (between ON and OFF)
        sensor_duration = {sensor['name']: [] for sensor in self.sensor_list}
        # dict[sensor, list of int] that stores the interval between sensor
        # deactivation to next activation
        sensor_interval = {sensor['name']: [] for sensor in self.sensor_list}
        for event in self.event_list:
            if prev_time[event['sensor_id']] is not None:
                if prev_state[event['sensor_id']] != event['sensor_status']:
                    if prev_state[event['sensor_id']] in start_status:
                        sensor_duration[event['sensor_id']].append(
                            (event['datetime'] - prev_time[
                                event['sensor_id']]).total_seconds())
                    else:
                        sensor_interval[event['sensor_id']].append(
                            (event['datetime'] - prev_time[
                                event['sensor_id']]).total_seconds())
            # Update previous state and time
            prev_state[event['sensor_id']] = event['sensor_status']
            prev_time[event['sensor_id']] = event['datetime']
        for sensor in sensor_duration.keys():
            sensor_duration[sensor] = sorted(sensor_duration[sensor],
                                             reverse=True)
        for sensor in sensor_interval.keys():
            sensor_interval[sensor] = sorted(sensor_interval[sensor],
                                             reverse=True)
        for sensor in sensor_duration.keys():
            print('%s: duration mid %f, mean %f, max %f, min %f' %
                  (sensor, np.median(sensor_duration[sensor]),
                   np.mean(sensor_duration[sensor]),
                   sensor_duration[sensor][0], sensor_duration[sensor][-1]))

    def save(self, fname, description=None):
        """Pickle the dataset structure for faster load and store

        Args:
            fname (:obj:`str`): The path to the pickle file which the dataset is
                saved to.
            description (:obj:`str`): Extra description of the dataset to be
                stored.
        """
        if description is not None:
            self.description = description
        import pickle
        fp = open(fname, 'wb')
        pickle.dump(self, fp, protocol=-1)
        fp.close()

    @staticmethod
    def load(fname):
        """Load from file where dataset is stored in pickle format.
        
        Args:
            fname (str): Path to the pickled file.
        
        Raises:
            FileNotFoundError: [description]
        
        Returns:
            CASASDataset: [description]
        """
        if not os.path.isfile(fname):
            raise FileNotFoundError("File %s not found." % fname)
        fp = open(fname, 'rb')
        import pickle
        dataset = pickle.load(fp)
        assert (isinstance(dataset, CASASDataset))
        print(
            "Dataset %s is loaded from file successfully." % dataset.data_dict[
                'name'])
        print(dataset.description)
        fp.close()
        return dataset

    def annotate_mrt(self, track_association, dir_path, dataset_name,
                     description, residents=None):
        """
        """
        if not os.path.isdir(dir_path):
            os.makedirs(dir_path)
        # Get Resident Info
        if residents is None or len(residents) == 0:
            if self.data_dict['residents'] is not None and len(
                    self.data_dict['residents']) > 0:
                residents = self.data_dict['residents']
            else:
                residents = []
        # Copy metadata
        metadata_filename = os.path.join(dir_path, "dataset.json")
        fp = open(metadata_filename, 'w')
        metadata_dict = copy.deepcopy(self.data_dict)
        metadata_dict['name'] = dataset_name
        metadata_dict['description'] = description
        if len(metadata_dict['residents']) == 0:
            metadata_dict['residents'] = residents
            for resident_dict in metadata_dict['residents']:
                if 'color' not in resident_dict:
                    resident_dict['color'] = "#FF000000"
        json.dump(metadata_dict, fp, indent=4)
        fp.close()
        # Temporary dictionary keep previous status of sensor
        dict_sensor_association = {sensor['name']: None for sensor in
                                   self.sensor_list}
        # go through event list and create new tagged event list
        i = 0
        event_filename = os.path.join(dir_path, "events.csv")
        fp = open(event_filename, 'w+')
        for event in self.event_list:
            resident_tag = None
            if event['message'] == "OFF" or \
                    event['message'] == 'ABSENT' or \
                    event['message'] == 'CLOSE':
                resident_tag = dict_sensor_association[event['sensor']]
            else:
                resident_tag = track_association[i][
                    self.sensor_indices_dict[event['sensor']]]
                dict_sensor_association[event['sensor']] = resident_tag
                i += 1
            event_time = event['datetime']
            event_time_string = event_time.strftime("%m/%d/%Y %H:%M:%S.% ") + \
                event_time.strftime("%z")[:3] + ":" + \
                event_time.strftime("%z")[3:]
            if resident_tag == -1 or resident_tag is None:
                resident_string = ""
            else:
                if len(residents) > 0:
                    resident_string = residents[resident_tag]['name']
                else:
                    resident_string = "R" + str(resident_tag)

            event_string = "%s,%s,%s,%s,,\n" % (event_time_string,
                                                event['sensor'],
                                                event['message'],
                                                resident_string)
            fp.write(event_string)
        fp.close()

    # region FeatureCalculation
    def _generate_sensor_sequence(self, ignore_off=True):
        sensor_seq = []
        time_seq = []
        activity_seq = []
        for event in self.event_list:
            if ignore_off and (event['message'] == "OFF" or
                     event['message'] == 'ABSENT' or
                     event['message'] == 'CLOSE'):
                continue
            else:
                sensor_seq.append(self.sensor_indices_dict[event['sensor']])
                time_seq.append(event['datetime'])
                if len(event['activity']) == 0:
                    activity_seq.append(-1)
                else:
                    activity_seq.append(
                        self.activity_indices_dict[event['activity'][0]]
                    )
        return sensor_seq, activity_seq, time_seq

    def _generate_sensor_observations(self):
        obs_seq = []  # to hold observations
        time_seq = []
        activity_seq = []
        # dictionary to store current states of all sensors
        sensor_status = {sensor['name']: 0 for sensor in self.sensor_list}
        sensor_info = {
            sensor['name']: None for sensor in self.sensor_list
        }

        with progressbar.ProgressBar(max_value=len(self.event_list)) as bar:
            # Go through all events
            for i, event in enumerate(self.event_list):
                if event['message'] == "OFF" or event['message'] == 'ABSENT' or \
                        event['message'] == 'CLOSE':
                    sensor_status[event['sensor']] = 0
                    if sensor_info[event['sensor']] is not None:
                        sensor_info[event['sensor']]['stop'] = event['datetime']
                        sensor_info[event['sensor']] = None
                else:
                    current_observation = {}
                    sensor_status[event['sensor']] = 1
                    if sensor_info[event['sensor']] is not None:
                        sensor_info[event['sensor']]['stop'] = event['datetime']
                    sensor_info[event['sensor']] = {
                        'start': event['datetime'],
                        'stop': None,
                        'residents': event['resident']
                    }

                    # Check for auto-shutdown
                    for sensor in self.sensor_list:
                        sensor_name = sensor['name']
                        if sensor_status[sensor_name] == 1:
                            sensor_id = self.sensor_indices_dict[sensor_name]
                            current_observation[sensor_id] = \
                                sensor_info[sensor_name]
                    obs_seq.append(current_observation)
                    time_seq.append(event['datetime'])
                    if len(event['activity']) == 0:
                        activity_seq.append(-1)
                    else:
                        activity_seq.append(
                            self.activity_indices_dict[event['activity'][0]]
                        )
                bar.update(i)
        return obs_seq, activity_seq, time_seq

    def _calculate_raw_features(self, normalized=True, sensor_one_hot=True):
        """Populate the feature vector with raw sensor data
        Args:
            normalized (:obj:`bool`): Will each feature be normalized between 0 and 1?
            sensor_one_hot (:obj:`bool`): For features related with sensor ID, are they
        """
        activities = [
            activity_name for activity_name in self.activity_dict
        ]
        sensors = [
            sensor['name'] for sensor in self.sensor_list
        ]
        if sensor_one_hot:
            len_per_event = 1 + len(sensors)
        else:
            len_per_event = 2
        sensor_seq, activity_seq, time_seq = self._generate_sensor_sequence(ignore_off=True)
        num_col = len_per_event
        num_events = len(sensor_seq)
        x = np.zeros((num_events, num_col), dtype=np.float32)
        y = np.zeros((num_events,), dtype=np.int32)
        for i in range(num_events):
            # Datetime is represented in seconds
            event_time = time_seq[i]
            seconds = event_time.timestamp() - \
                datetime.combine(event_time.date(), time(0)).timestamp()
            if normalized:
                x[i, 0] = seconds/(24*3600)
            else:
                x[i, 0] = seconds
            # Sensor id
            sensor_index = sensor_seq[i]
            if sensor_one_hot:
                x[i, sensor_index + 1] = 1
            else:
                x[i, 1] = sensor_index
            y[i] = activity_seq[i]
        time_list = time_seq
        return activities, sensors, x, y, time_list

    def _calculate_obs_features(self, normalized=True):
        """Populate the feature vector with current active sensors.
        """
        activities = [
            activity_name for activity_name in self.activity_dict
        ]
        sensors = [
            sensor['name'] for sensor in self.sensor_list
        ]
        obs_seq, activity_seq, time_seq = self._generate_sensor_observations()
        num_col = 1 + len(sensors)
        num_events = len(obs_seq)
        x = np.zeros((num_events, num_col), dtype=np.float32)
        y = np.zeros((num_events,), dtype=np.int32)
        for i in range(num_events):
            # Datetime is represented in seconds
            event_time = time_seq[i]
            seconds = event_time.timestamp() - \
                datetime.combine(event_time.date(), time(0)).timestamp()
            if normalized:
                x[i, 0] = seconds/(24*3600)
            else:
                x[i, 0] = seconds
            # Sensor id
            for sensor_index in obs_seq[i]:
                x[i, sensor_index + 1] = 1
            y[i] = activity_seq[i]
        time_list = time_seq
        return activities, sensors, x, y, time_list

    def _calculate_stats_feature(self, window_size=30, normalized=True, sensor_one_hot=True, enabled_features=None):
        if enabled_features is None:
            from ._stats import EventHour
            from ._stats import EventSeconds
            from ._stats import LastSensor
            from ._stats import WindowDuration
            from ._stats import SensorCount
            from ._stats import DominantSensor
            from ._stats import SensorElapseTime
            enabled_features = [
                EventHour(normalized=normalized),
                EventSeconds(normalized=normalized),
                LastSensor(sensor_one_hot=sensor_one_hot),
                WindowDuration(normalized=normalized),
                SensorCount(normalized=normalized),
                DominantSensor(sensor_one_hot=sensor_one_hot),
                SensorElapseTime(normalized=normalized)
            ]

        def _reorder_features(features):
            static_features = []
            dynamic_features = []
            for feature in features:
                if feature.sensor_one_hot:
                    dynamic_features.append(feature)
                else:
                    static_features.append(feature)
            return static_features + dynamic_features, len(static_features), len(dynamic_features)

        enabled_features, num_static_features, num_dynamic_features = _reorder_features(enabled_features)
        num_enabled_sensors = len(self.sensor_list)

        sensor_info = OrderedDict()
        for sensor in self.sensor_list:
            sensor_info[sensor['name']] = {
                'index': self.sensor_indices_dict[sensor['name']],
                'enable': True
            }

        # Populate Feature and Feature routines
        feature_list = OrderedDict()
        feature_routines = OrderedDict()
        for feature in enabled_features:
            if feature.name not in feature_list:
                feature_list[feature.name] = feature
                if feature.routine is not None and feature.routine.name not in feature_routines:
                    feature_routines[feature.routine.name] = feature.routine

        activities = [
            activity_name for activity_name in self.activity_dict
        ]

        features = []
        for i, feature_name in enumerate(feature_list):
            feature = feature_list[feature_name]
            if feature.sensor_one_hot:
                features += [
                    '%s-%s' % (feature.name, sensor_name) for sensor_name in sensor_info
                ]
            else:
                features.append(feature.name)

        def _count_features():
            """Count the size of feature columns

            Returns:
                :obj:`int`: size of feature columns
            """
            num_feature_columns = 0
            for feature_name in feature_list:
                if feature_list[feature_name].enabled:
                    if feature_list[feature_name].sensor_one_hot:
                        num_feature_columns += num_enabled_sensors
                    else:
                        num_feature_columns += 1
            return num_feature_columns

        # Prepare datalist
        data_list = [
            event for event in self.event_list
            if not (event['message'] == "OFF" or event['message'] == 'ABSENT' or
                    event['message'] == 'CLOSE')
        ]

        num_cols = _count_features()
        num_rows = len(data_list) - window_size + 1
        x = np.zeros((num_rows, num_cols), dtype=np.float32)
        y = np.zeros((num_rows,), dtype=np.int32)
        time_list = []
        event_i = 0
        row_i = 0
        # Reset existing data within feature routines.
        for routine_name in feature_routines:
            routine = feature_routines[routine_name]
            if routine.enabled:
                routine.clear()
        # Start calculation
        with progressbar.ProgressBar(max_value=len(data_list)) as bar:
            while event_i < len(data_list):
                for routine_name in feature_routines:
                    routine = feature_routines[routine_name]
                    if routine.enabled:
                        routine.update(
                            data_list=data_list,
                            cur_index=event_i,
                            window_size=window_size,
                            sensor_info=sensor_info
                        )
                # Get feature data and populate x
                for feature_index, feature_name in enumerate(feature_list):
                    feature = feature_list[feature_name]
                    if feature.sensor_one_hot:
                        for sensor_name in sensor_info:
                            col_i = num_static_features + (feature_index - num_static_features) * num_enabled_sensors \
                                    + self.sensor_indices_dict[sensor_name]
                            x[row_i, col_i] = feature.get_feature_value(
                                data_list=data_list,
                                cur_index=event_i,
                                window_size=window_size,
                                sensor_info=sensor_info,
                                sensor_name=sensor_name
                            )
                    else:
                        col_i = feature_index
                        x[row_i, col_i] = feature.get_feature_value(
                            data_list=data_list,
                            cur_index=event_i,
                            window_size=window_size,
                            sensor_info=sensor_info,
                            sensor_name=None
                        )
                y[row_i] = self.activity_indices_dict[data_list[event_i]['activity'][0]]
                if event_i >= window_size - 1:
                    time_list.append(data_list[event_i]['datetime'])
                    row_i += 1
                event_i += 1
                bar.update(event_i)
        return activities, features, x, y, time_list

    @staticmethod
    def _break_by_day(time_list):
        """Find the split point of the dataset by day
        Returns:
            :obj:`list` of :obj:`int`: List of indices of the event.rst at the beginning of each day
        """
        day_index_list = [0]
        start_date = time_list[0].date()
        for i in range(len(time_list)):
            cur_date = time_list[i].date()
            if cur_date > start_date:
                day_index_list.append(i)
                start_date = cur_date
        day_index_list.append(len(time_list))
        return day_index_list

    @staticmethod
    def _break_by_week(time_list):
        """Find the split point of the dataset by week
        Returns:
            :obj:`list` of :obj:`int`: List of indices of the event.rst at the beginning of each week
        """
        week_index_list = [0]
        start_date = time_list[0].date()
        for i in range(len(time_list)):
            cur_date = time_list[i].date()
            # Monday - then not the same day as start_date
            # Else, if more than 7 days apart
            if (cur_date.weekday() == 0 and cur_date > start_date) or (cur_date - start_date).days >= 7:
                week_index_list.append(i)
                start_date = cur_date
        week_index_list.append(len(time_list))
        return week_index_list

    def export_hdf5(self, filename, comments='', bg_activity='Other_Activity',
                    mode='raw', normalized=True, sensor_one_hot=True, window_size=30,
                    driver=None):
        if mode == "raw":
            activities, sensors, x, y, time_list = self._calculate_raw_features(
                normalized=normalized,
                sensor_one_hot=sensor_one_hot
            )
            feature_description = ['timestamp']
            if sensor_one_hot:
                feature_description += [
                    sensor for sensor in sensors
                ]
            else:
                feature_description += ['sensors']
        elif mode == "observation":
            activities, sensors, x, y, time_list = self._calculate_obs_features(
                normalized=normalized
            )
            feature_description = ['timestamp'] + [
                sensor for sensor in sensors
            ]
        elif mode == "stats":
            activities, feature_description, x, y, time_list = self._calculate_stats_feature(
                window_size=window_size,
                normalized=normalized,
                sensor_one_hot=sensor_one_hot
            )
        else:
            raise NotImplementedError
        target_description = activities
        target_colors = [
            self.get_activity_color(activity) for activity in activities
        ]
        from ._hdf import CASASHDF5
        casas_hdf5 = CASASHDF5(filename, mode='w', driver=driver)
        casas_hdf5.create_features(feature_array=x,
                                   feature_description=feature_description)
        casas_hdf5.create_targets(target_array=y,
                                  target_description=target_description,
                                  target_colors=target_colors)
        casas_hdf5.create_time_list(time_array=time_list)
        casas_hdf5.create_splits(days=CASASDataset._break_by_day(time_list=time_list),
                                 weeks=CASASDataset._break_by_week(time_list=time_list))
        casas_hdf5.create_comments(comments)
        casas_hdf5.create_sensors(sensors=[sensor['name'] for sensor in self.sensor_list])
        if bg_activity is not None:
            casas_hdf5.set_background_target(bg_activity)
        casas_hdf5.flush()
        casas_hdf5.close()

    def export_xlsx(self, filename, comments='', bg_activity='Other_Activity',
                    mode='raw', normalized=True, sensor_one_hot=True, window_size=30,
                    start=0, end=-1):
        if mode == "raw":
            activities, sensors, x, y, time_list = self._calculate_raw_features(
                normalized=normalized,
                sensor_one_hot=sensor_one_hot
            )
            feature_description = ['timestamp']
            if sensor_one_hot:
                feature_description += [
                    sensor for sensor in sensors
                ]
            else:
                feature_description += ['sensors']
        elif mode == "observation":
            activities, sensors, x, y, time_list = self._calculate_obs_features(
                normalized=normalized
            )
            feature_description = ['timestamp'] + [
                sensor for sensor in sensors
            ]
        elif mode == "stats":
            activities, feature_description, x, y, time_list = self._calculate_stats_feature(
                window_size=window_size,
                normalized=normalized,
                sensor_one_hot=sensor_one_hot
            )
        else:
            raise NotImplementedError
        logger.debug('Start writing xlsx workbook.')
        workbook = xlsxwriter.Workbook(filename)
        # Dump Activities
        activity_sheet = workbook.add_worksheet("Activities")
        c = 0
        for item in self.activity_dict[list(self.activity_dict.keys())[0]].keys():
            activity_sheet.write(0, c, str(item))
            c += 1
        r = 1
        for activity in self.activity_dict:
            c = 0
            for item in self.activity_dict[activity].keys():
                activity_sheet.write(r, c, str(self.activity_dict[activity][item]))
                c += 1
            r += 1
        # Dump Sensors
        sensor_sheet = workbook.add_worksheet("Sensors")
        c = 0
        for item in self.sensor_list[0].keys():
            sensor_sheet.write(0, c, str(item))
            c += 1
        r = 1
        for sensor in self.sensor_list:
            c = 0
            for item in sensor.keys():
                sensor_sheet.write(r, c, str(sensor[item]))
                c += 1
            r += 1
        # Get range to dump
        num_rows, num_cols = x.shape
        if end == -1:
            end = num_rows
        # Dump Events
        event_sheet = workbook.add_worksheet('Events')
        event_titles = ['timestamp', 'sensor', 'message', 'resident', 'activity']
        for c, title in enumerate(event_titles):
            event_sheet.write(0, c, title)
        start_time = time_list[start]
        end_time = time_list[end - 1]
        event_i = window_size - 1
        while event_i < len(self.event_list):
            event = self.event_list[event_i]
            if event['datetime'] < start_time:
                event_i += 1
            else:
                break
        event_start = event_i - window_size + 1
        while event_i < len(self.event_list):
            event = self.event_list[event_i]
            if event['datetime'] < end_time:
                event_i += 1
            else:
                break
        event_end = event_i
        r = 1
        for i in range(event_start, event_end):
            event = self.event_list[i]
            time_string = event['datetime'].strftime("%m/%d/%Y %H:%M:%S.%f ") + \
                          event['datetime'].strftime("%z")[:3] + ":" + \
                          event['datetime'].strftime("%z")[3:]
            event_sheet.write(r, 0, time_string)
            event_sheet.write(r, 1, event['sensor'])
            event_sheet.write(r, 2, event['message'])
            event_sheet.write(r, 3, '; '.join(event['resident']))
            event_sheet.write(r, 4, '; '.join(event['activity']))
            r += 1
        # Dump Data
        data_sheet = workbook.add_worksheet('Data')
        # Export self.x feature
        data_sheet.write(0, 0, 'activity')
        data_sheet.write(0, 1, 'activity_id')
        # Add Feature Title
        for c, feature_title in enumerate(feature_description):
            data_sheet.write(0, c+2, feature_title)
        r = 1
        if start < num_rows and start < end:
            for i in range(start, end):
                data_sheet.write(r, 1, y[i])
                data_sheet.write(r, 0, activities[y[i]])
                for j in range(num_cols):
                    data_sheet.write(r, j+2, x[i, j])
                r += 1
        workbook.close()
